FROM rust:1-buster as planner
WORKDIR /var/app
RUN cargo install cargo-chef 
COPY src /var/app/src
COPY Cargo.lock Cargo.toml /var/app/
RUN cargo chef prepare --recipe-path recipe.json

FROM rust:1-buster as cacher
WORKDIR /var/app
COPY --from=planner /usr/local/cargo /usr/local/cargo
COPY --from=planner /var/app/recipe.json recipe.json
RUN cargo chef cook --release --recipe-path recipe.json


FROM rust:1-buster as builder
WORKDIR /var/app
COPY --from=cacher /var/app/target target
COPY --from=cacher /usr/local/cargo /usr/local/cargo
COPY src /var/app/src
COPY Cargo.lock Cargo.toml /var/app/
RUN cargo build --release

FROM debian:buster
WORKDIR /var/app
COPY --from=builder /var/app/target/release/book-admin /bin/book-admin
RUN chmod +x /bin/book-admin
RUN /bin/book-admin --help

EXPOSE 8080
ENTRYPOINT ["/bin/book-admin"]
CMD ["--config", "/secrets/local.env.yaml", "-vvv", "serve", "--bind", "0.0.0.0", "--port", "8080", "--migrate", "--load"]
