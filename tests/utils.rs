use assert_fs::{prelude::*, NamedTempFile};
use std::{
    io::{BufRead, BufReader},
    process::{ChildStdout, Command},
    time::{Duration, Instant},
};

#[must_use]
pub fn get_tmp_cfg(mongo_host: &str, log_level: &str) -> NamedTempFile {
    let tmp_cfg = NamedTempFile::new("tmp_config.yaml").unwrap();
    tmp_cfg
        .write_str(&format!(
            r#"---
env: dev
mongodb:
    srv: False
    host: '{mongo_host}'
    authentication-database: 'book-admin'
    user: book-admin
    password: '2vQU45haQCnDS8sO'
    database: 'book-admin'
    replica-set: ''
logger:
    level: {log_level}
"#
        ))
        .unwrap();
    tmp_cfg
}

pub fn read_stdout_until_contains(
    bufread: &mut BufReader<ChildStdout>,
    pat: &str,
    timeout: u64,
) -> Option<()> {
    let start = Instant::now();
    let mut buf = String::new();
    while let Ok(n) = bufread.read_line(&mut buf) {
        let elapsed = start.elapsed();
        if elapsed > Duration::from_secs(timeout) {
            return None;
        }
        if n > 0 {
            if buf.contains(pat) {
                return Some(());
            }
            buf.clear();
        }
    }
    None
}

pub fn send_sigint(pid: u32) {
    Command::new("kill")
        .args(["-SIGINT", &pid.to_string()])
        .spawn()
        .unwrap();
}
