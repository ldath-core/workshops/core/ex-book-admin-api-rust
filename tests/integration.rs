use assert_cmd::{cargo::CommandCargoExt, Command};
use predicates::prelude::*;
use std::{io::BufReader, process::Stdio, time::Duration};

mod utils;
use utils::{get_tmp_cfg, read_stdout_until_contains, send_sigint};

#[test]
fn help_command_succeeds() {
    let mut cmd = Command::cargo_bin("book-admin").unwrap();
    cmd.timeout(Duration::from_secs(1));
    cmd.args(["serve", "--help"]);
    cmd.assert().success();
}

#[test]
fn nonexistent_subcommand_fails() {
    let mut cmd = Command::cargo_bin("book-admin").unwrap();
    cmd.timeout(Duration::from_secs(1));
    cmd.args(["this-subcommand-does-not-exist"]);
    cmd.assert().failure();
}

#[test]
fn default_log_level_from_config() {
    let cfg = get_tmp_cfg("cause-fail", "debug");
    let mut cmd = Command::cargo_bin("book-admin").unwrap();
    cmd.timeout(Duration::from_secs(1));
    cmd.args(["--config", cfg.path().to_str().unwrap(), "serve"]);

    cmd.assert().stdout(
        predicate::str::contains(r#""verbosity":"Level(Debug)""#)
            .and(predicate::str::contains(r#""format":"Json""#)),
    );
}

#[test]
fn overwrite_log_level_from_cli() {
    let cfg = get_tmp_cfg("cause-fail", "debug");
    let mut cmd = Command::cargo_bin("book-admin").unwrap();
    cmd.timeout(Duration::from_secs(1));
    cmd.args(["--config", cfg.path().to_str().unwrap(), "-vv", "serve"]);
    cmd.assert()
        .stdout(predicate::str::contains(r#""verbosity":"Level(Trace)""#));
}

#[test]
fn server_starts_and_shutdowns_gracefuly() {
    let cfg = get_tmp_cfg("localhost:27017", "debug");

    let mut cmd = std::process::Command::cargo_bin("book-admin").unwrap();
    cmd.args([
        "--config",
        cfg.path().to_str().unwrap(),
        "serve",
        "--port",
        "8083",
    ]);
    let mut running = cmd.stdout(Stdio::piped()).spawn().unwrap();
    let mut bufread = BufReader::new(running.stdout.take().unwrap());

    read_stdout_until_contains(&mut bufread, "Serve params", 15).unwrap();
    send_sigint(running.id());
    read_stdout_until_contains(&mut bufread, "starting graceful shutdown", 15).unwrap();
}
