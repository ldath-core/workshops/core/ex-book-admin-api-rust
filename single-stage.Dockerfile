FROM rust:1.67

WORKDIR /var/app
COPY src /var/app/src
COPY Cargo.lock Cargo.toml /var/app/
RUN cargo build --release 
RUN cp target/release/book-admin /bin/book-admin

EXPOSE 8080

ENTRYPOINT ["/bin/book-admin"]
CMD ["--config", "/secrets/local.env.yaml", "-vvv", "serve", "--bind", "0.0.0.0", "--port", "8080", "--migrate", "--load"]
