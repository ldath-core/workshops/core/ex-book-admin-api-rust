#!/usr/bin/env bash
set -e
kubectx k3d-bookCluster
./build-multi-stage-container.sh
# uncomment if using podman
#podman tag book-admin:latest docker.io/book-admin:latest 
k3d image import -c bookCluster docker.io/library/book-admin:latest
helm upgrade -i -f secrets/k3s-values.yaml --create-namespace --namespace=services k3s-book-admin ex-book/book-service --version 0.4.0 --dry-run --debug
helm upgrade -i -f secrets/k3s-values.yaml --create-namespace --namespace=services k3s-book-admin ex-book/book-service --version 0.4.0 --wait
