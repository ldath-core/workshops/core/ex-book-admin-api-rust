#!/usr/bin/env bash
./target/release/book-admin --config ./secrets/local.env.yaml --logger-format json serve -b 127.0.0.1 -p 8883 --migrate --load ##--cors
