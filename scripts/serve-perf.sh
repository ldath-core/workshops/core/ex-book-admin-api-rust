#!/usr/bin/env bash
# perf record --call-graph=dwarf -s -- ./target/release/book-admin --config ./secrets/local.env.yaml --logger-format full -v serve -b 127.0.0.1 -p 8883 --migrate --load --cors
perf record --call-graph=dwarf -s -g -e cycles,instructions,task-clock,cpu-clock -- ../target/release/book-admin --config ../secrets/local.env.yaml --logger-format full -v serve -b 127.0.0.1 -p 8883 --migrate --load --cors
