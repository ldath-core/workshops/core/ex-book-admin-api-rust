#!/usr/bin/bash
echo "starting tasks"
FAILS=0
for j in {1..100}; do
    for i in {1..10}; do
        # ./make-traffic.sh &
        ./make-traffic-no-pass-hashing.sh &
        pids[${i}]=$!
    done
    echo -e "\nstarted a batch of tasks"
    for pid in ${pids[*]}; do
        wait $pid
        if [ "$?" != "0" ]; then
            FAILS=$((FAILS+1))
        fi
    done
    echo -e "\nbatch finished"
done
echo -e "\nnumber of fails : $FAILS"
echo -e "\n\nALL DONE"

# or
# wait < <(jobs -p)
