#!/usr/bin/bash
curl -s -o /dev/null -X 'GET' \
  'http://localhost:8883/v1/health' \
  -H 'accept: application/json'

ID=$(curl -s -X 'GET' \
  'http://localhost:8883/v1/admins?skip=0&limit=10' \
  -H 'accept: application/json' | jq '.content.results[0].id' | tr -d '"')

curl -s -o /dev/null -X 'PUT' \
  "http://localhost:8883/v1/admins/$ID" \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{"lastName": "updated lastname$RANDOM"}'

curl -s -o /dev/null -X 'GET' \
  "http://localhost:8883/v1/admins/$ID" \
  -H 'accept: application/json'

curl -s -o /dev/null -X 'PUT' \
  "http://localhost:8883/v1/admins/$ID" \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{"lastName": "updated lastname$RANDOM"}'

curl -s -o /dev/null -X 'GET' \
  "http://localhost:8883/v1/admins/$ID" \
  -H 'accept: application/json'

curl -s -o /dev/null -X 'PUT' \
  "http://localhost:8883/v1/admins/$ID" \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{"lastName": "updated lastname$RANDOM"}'

STATUS=$(curl -s -X 'GET' \
  "http://localhost:8883/v1/admins/$ID" \
  -H 'accept: application/json' | jq '.status')

echo "last get status: $STATUS"
if [ "$STATUS" != "200" ]; then
  exit 112
fi

