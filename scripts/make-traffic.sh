#!/usr/bin/bash
curl -s -o /dev/null -X 'GET' \
  'http://localhost:8883/v1/health' \
  -H 'accept: application/json'

ID=$(curl -s -X 'POST' \
  'http://localhost:8883/v1/admins' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d "{
  \"email\": \"string$RANDOM$RANDOM@test.pl\",
  \"firstName\": \"string\",
  \"lastName\": \"string\",
  \"password\": \"string\",
  \"version\": 1
}" | jq '.content.id' | tr -d '"')

echo "ID: $ID"
if [ "$ID" == "" ]; then
  exit 111
fi

curl -s -o /dev/null -X 'GET' \
  'http://localhost:8883/v1/admins?skip=0&limit=10' \
  -H 'accept: application/json'

curl -s -o /dev/null -X 'PUT' \
  "http://localhost:8883/v1/admins/$ID" \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{"lastName": "updated lastname"}'

curl -s -o /dev/null -X 'GET' \
  "http://localhost:8883/v1/admins/$ID" \
  -H 'accept: application/json'

STATUS=$(curl -s -X 'DELETE' \
  "http://localhost:8883/v1/admins/$ID" \
  -H 'accept: application/json' | jq '.status')

echo "delete status: $STATUS"
if [ "$STATUS" != "202" ]; then
  exit 112
fi