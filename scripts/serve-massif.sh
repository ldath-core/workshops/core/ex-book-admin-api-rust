#!/usr/bin/env bash
valgrind --tool=massif ../target/release/book-admin --config ../secrets/local.env.yaml --logger-format full -v serve -b 127.0.0.1 -p 8883 --migrate --load --cors
