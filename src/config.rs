use serde::{Deserialize, Serialize};
use serde_yaml::{self};
use std::path::Path;
use tracing::{debug, instrument};

#[cfg(test)]
mod tests;

/// Reads yaml file and creates Config struct for App to use
///
/// Example config file:
/// ```yaml
///---
///env: dev
///mongodb:
///  srv: False
///  host: 'localhost:27017'
///  authentication-database: 'book-admin'
///  user: book-admin
///  password: 'password123'
///  database: 'book-admin'
///  replica-set: ''
///logger:
///  level: debug
/// ```
#[instrument(level = "trace")]
pub fn load_config(path: &Path) -> Config {
    let f = std::fs::File::open(path)
        .unwrap_or_else(|_| panic!("could not open config source file {}", path.display()));
    let config: Config = serde_yaml::from_reader(f).expect("could not parse config at.");
    debug!("Config {} loaded", path.display());
    config
}

/// Config struct for App with all its subelements
#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub env: String,
    pub mongodb: MongoDBConfig,
    pub logger: LoggerConfig,
}

#[cfg_attr(test, derive(Clone))]
#[derive(Debug, Serialize, Deserialize)]
pub struct MongoDBConfig {
    pub srv: bool,
    pub host: String,
    #[serde(rename = "authentication-database")]
    pub authentication_database: String,
    pub user: String,
    pub password: String,
    pub database: String,
    #[serde(rename = "replica-set")]
    pub replica_set: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct LoggerConfig {
    pub level: String,
}
