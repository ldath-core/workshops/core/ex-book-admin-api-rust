use http::{HeaderValue, Request, Response, StatusCode, Uri};
use hyper::Body;
use mongodb::bson::oid::ObjectId;

pub mod utils;
use crate::{
    db::Admin,
    router::{NewAdmin, UpdateAdmin},
};
use utils::{
    body_as_json, body_as_string, create_random_database, drop_database, get_random_port_listener,
    retry_request, start_background_server, uri,
};

const LISTEN_IP: &str = "127.0.0.1";

struct TestContext {
    db_name: String,
}

impl TestContext {
    async fn new() -> Self {
        Self {
            db_name: create_random_database().await,
        }
    }
}

impl Drop for TestContext {
    fn drop(&mut self) {
        let h = match tokio::runtime::Handle::try_current() {
            Ok(h) => h,
            Err(_) => tokio::runtime::Runtime::new().unwrap().handle().clone(),
        };
        let db_name = self.db_name.clone();
        tokio::task::block_in_place(move || {
            h.block_on(drop_database(&db_name));
        });
    }
}

#[tokio::test(flavor = "multi_thread")]
async fn test_get_health() {
    let ctx = TestContext::new().await;
    let (listener, addr) = get_random_port_listener(LISTEN_IP);

    let _tx = start_background_server(listener, &ctx.db_name).await;

    let resp = retry_request(uri(addr, "/v1/health"), 10).await.unwrap();
    assert_eq!(resp.status(), StatusCode::OK);
    assert_eq!(
        resp.headers().get("content-type").unwrap(),
        &HeaderValue::from_static("application/json")
    );

    let body = body_as_string(resp).await;
    assert!(body.contains("\"alive\":true"));
}

#[tokio::test(flavor = "multi_thread")]
async fn test_get_admins() {
    let ctx = TestContext::new().await;
    let (listener, addr) = get_random_port_listener(LISTEN_IP);

    let _tx = start_background_server(listener, &ctx.db_name).await;

    let resp = retry_request(uri(addr, "/v1/admins"), 10).await.unwrap();
    assert_eq!(resp.status(), StatusCode::OK);

    let body = body_as_json(resp).await;
    let admins = body["content"]["results"].as_array().unwrap().clone();
    assert!(!admins.is_empty());
    for admin in admins {
        let _x: Admin = serde_json::from_value(admin).unwrap();
    }
}

#[tokio::test(flavor = "multi_thread")]
async fn test_create_and_get_admin() {
    let tested_admin = get_new_admin;

    let ctx = TestContext::new().await;
    let (listener, addr) = get_random_port_listener(LISTEN_IP);
    let _tx = start_background_server(listener, &ctx.db_name).await;
    let resp = send_req(
        Request::post(uri(addr, "/v1/admins"))
            .header("content-type", "application/json")
            .body(Body::from(serde_json::to_string(&tested_admin()).unwrap()))
            .unwrap(),
    )
    .await;
    assert_eq!(resp.status(), StatusCode::CREATED);
    let body = body_as_json(resp).await;
    let created_id = body["content"]["id"].as_str().unwrap();

    let resp = send_req(get(uri(addr, &format!("/v1/admins/{created_id}")))).await;
    assert_eq!(resp.status(), StatusCode::OK);

    let body = body_as_json(resp).await;
    assert_eq!(
        body["content"]["email"].as_str().unwrap(),
        &tested_admin().email
    );
    assert_eq!(
        body["content"]["firstName"].as_str().unwrap(),
        &tested_admin().first_name.unwrap()
    );
    assert_eq!(
        body["content"]["lastName"].as_str().unwrap(),
        &tested_admin().last_name.unwrap()
    );
}

#[tokio::test(flavor = "multi_thread")]
async fn test_create_update_and_get_admin() {
    let tested_new_admin = get_new_admin;
    let tested_update_admin = get_update_admin;

    let ctx = TestContext::new().await;
    let (listener, addr) = get_random_port_listener(LISTEN_IP);
    let _tx = start_background_server(listener, &ctx.db_name).await;

    let resp = send_req(
        Request::post(uri(addr, "/v1/admins"))
            .header("content-type", "application/json")
            .body(Body::from(
                serde_json::to_string(&tested_new_admin()).unwrap(),
            ))
            .unwrap(),
    )
    .await;
    assert_eq!(resp.status(), StatusCode::CREATED);
    let body = body_as_json(resp).await;
    let created_id = body["content"]["id"].as_str().unwrap();

    let resp = send_req(
        Request::put(uri(addr, &format!("/v1/admins/{created_id}")))
            .header("content-type", "application/json")
            .body(Body::from(
                serde_json::to_string(&tested_update_admin()).unwrap(),
            ))
            .unwrap(),
    )
    .await;
    assert_eq!(resp.status(), StatusCode::ACCEPTED);

    let resp = send_req(get(uri(addr, &format!("/v1/admins/{created_id}")))).await;
    assert_eq!(resp.status(), StatusCode::OK);

    let body = body_as_json(resp).await;
    assert_eq!(
        body["content"]["email"].as_str().unwrap(),
        &tested_update_admin().email.unwrap()
    );
    assert_eq!(
        body["content"]["firstName"].as_str().unwrap(),
        &tested_update_admin().first_name.unwrap()
    );
    assert_eq!(
        body["content"]["lastName"].as_str().unwrap(),
        &tested_update_admin().last_name.unwrap()
    );
}

#[tokio::test(flavor = "multi_thread")]
async fn test_delete_admin() {
    let ctx = TestContext::new().await;
    let (listener, addr) = get_random_port_listener(LISTEN_IP);
    let _tx = start_background_server(listener, &ctx.db_name).await;

    let resp = send_req(get(uri(addr, "/v1/admins"))).await;
    assert_eq!(resp.status(), StatusCode::OK);

    let body = body_as_json(resp).await;
    let admins = body["content"]["results"].as_array().unwrap().clone();
    let delete_id = admins[0]["id"].as_str().unwrap();

    let resp = send_req(del(uri(addr, &format!("/v1/admins/{delete_id}")))).await;
    assert_eq!(resp.status(), StatusCode::ACCEPTED);
}

#[tokio::test(flavor = "multi_thread")]
async fn test_wrong_endpoint() {
    let ctx = TestContext::new().await;
    let (listener, addr) = get_random_port_listener(LISTEN_IP);
    let _tx = start_background_server(listener, &ctx.db_name).await;

    let resp = send_req(get(uri(addr, "/nonexistentendpoint"))).await;
    let _body = body_as_json(resp).await;
}

#[tokio::test(flavor = "multi_thread")]
async fn test_good_endpoint_wrong_method() {
    let ctx = TestContext::new().await;
    let (listener, addr) = get_random_port_listener(LISTEN_IP);
    let _tx = start_background_server(listener, &ctx.db_name).await;

    let resp = send_req(
        Request::delete(uri(addr, "/v1/admins"))
            .header("content-type", "application/json")
            .body(Body::empty())
            .unwrap(),
    )
    .await;
    assert_eq!(resp.status(), StatusCode::METHOD_NOT_ALLOWED);
    let _body = body_as_json(resp).await;
}

#[tokio::test(flavor = "multi_thread")]
async fn test_get_nonexistent_admin() {
    let ctx = TestContext::new().await;
    let (listener, addr) = get_random_port_listener(LISTEN_IP);
    let _tx = start_background_server(listener, &ctx.db_name).await;

    let random_oid = ObjectId::new().to_string();
    let resp = send_req(get(uri(addr, &format!("/v1/admins/{random_oid}")))).await;
    assert_eq!(resp.status(), StatusCode::INTERNAL_SERVER_ERROR);
}

#[tokio::test(flavor = "multi_thread")]
async fn test_get_admin_malformed_id() {
    let ctx = TestContext::new().await;
    let (listener, addr) = get_random_port_listener(LISTEN_IP);
    let _tx = start_background_server(listener, &ctx.db_name).await;

    let resp = send_req(get(uri(addr, "/v1/admins/notMongoObjectId"))).await;
    assert_eq!(resp.status(), StatusCode::BAD_REQUEST);
    let body = body_as_string(resp).await;
    assert!(body.contains("invalid value"));
}

fn get(uri: Uri) -> Request<Body> {
    Request::get(uri).body(Body::empty()).unwrap()
}

fn del(uri: Uri) -> Request<Body> {
    Request::delete(uri).body(Body::empty()).unwrap()
}

async fn send_req(req: Request<Body>) -> Response<Body> {
    let client = hyper::Client::new();
    client.request(req).await.unwrap()
}

fn get_new_admin() -> NewAdmin {
    NewAdmin {
        email: "email0@domain.com".to_string(),
        first_name: Some("firstname0".to_string()),
        last_name: Some("lastname0".to_string()),
        password: "passhash0".to_string(),
        version: None,
    }
}

fn get_update_admin() -> UpdateAdmin {
    UpdateAdmin {
        email: Some("updated@email.com".to_string()),
        first_name: Some("updatedfirstname".to_string()),
        last_name: Some("updatedlastname".to_string()),
        password: None,
        version: None,
    }
}
