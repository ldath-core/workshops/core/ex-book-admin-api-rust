#![forbid(unsafe_code)]
#![deny(clippy::all)]
//! book-admin is an API server for performing CRUD
//! operations on a simple admin data scheme.
//!
//! Implemented for the sake of [microservices-workshop-be](https://gitlab.com/mobica-workshops/documentation/microservices-backend-local-development-guide)
//!
//! It's based on [go book-admin implementation](https://gitlab.com/mobica-workshops/examples/go/gorilla/book-admin)
//!
use anyhow::Result;
use std::net::SocketAddr;
use tracing::{debug, error, info, Level};
use tracing_subscriber::{fmt::writer::MakeWriterExt, layer::SubscriberExt};

mod cli;
mod config;
mod db;
mod router;
mod traits;

#[cfg(test)]
mod tests;

use crate::{
    cli::{generate_completion, parse_args, Commands, LoggerFormat},
    config::load_config,
    db::Db,
    router::{add_cors_layer, get_router},
    traits::DatabaseConnector,
};

#[tokio::main]
async fn main() -> Result<()> {
    let cli = parse_args();
    let config = load_config(cli.config.as_path());
    init_logger(cli.verbose, &cli.logger_format, &config.logger.level);
    let db = Db::new(Db::connect(&config.mongodb).await?, config.mongodb.database).await?;
    let mut router = get_router(db.clone());

    match &cli.command {
        Commands::Load => {
            db.create_test_data().await?;
            info!("Loaded test data into DB");
        }
        Commands::Migrate => {
            db.migrate_db().await?;
            info!("Migrated DB");
        }
        Commands::Serve(s) => {
            debug!("Serve params: {:?}", s);
            if s.cors {
                router = add_cors_layer(router);
            }
            if s.migrate {
                db.migrate_db().await?;
            }
            if s.load {
                db.create_test_data().await?;
            }
            let addr = SocketAddr::from((s.bind, s.port));
            info!("Starting to listen on addr: {addr:#?}");
            let server = axum::Server::bind(&addr)
                .serve(router.into_make_service())
                .with_graceful_shutdown(router::shutdown_signal());
            if let Err(e) = server.await {
                error!("Server returned error {:?}", e);
            }
            db.close().await;
        }
        Commands::Completion => {
            generate_completion();
        }
    }
    Ok(())
}

/// Initialize logger with variable format and verbosity
/// Verbosity is decided based on `config` if no `-v` parameter was passed
/// otherwise it is overwritten.
/// Default level is "INFO"
fn init_logger(verbose: u8, format: &LoggerFormat, config: &str) {
    let verbosity = match verbose {
        0 => match config {
            "debug" => Level::DEBUG,
            "trace" => Level::TRACE,
            "error" => Level::ERROR,
            "warn" => Level::WARN,
            _ => Level::INFO,
        },
        1 => Level::DEBUG,
        _ => Level::TRACE,
    };

    let tracing_layer = tracing_subscriber::fmt::Layer::new()
        .with_writer(std::io::stdout.with_max_level(verbosity))
        .with_span_events(tracing_subscriber::fmt::format::FmtSpan::CLOSE);
    match format {
        LoggerFormat::Json => {
            tracing::subscriber::set_global_default(
                tracing_subscriber::registry().with(tracing_layer.json()),
            )
            .expect("Failed to set up global json subscriber");
        }
        LoggerFormat::Pretty => {
            tracing::subscriber::set_global_default(
                tracing_subscriber::registry().with(tracing_layer.pretty()),
            )
            .expect("Failed to set up global pretty subscriber");
        }
        LoggerFormat::Compact => {
            tracing::subscriber::set_global_default(
                tracing_subscriber::registry().with(tracing_layer.compact()),
            )
            .expect("Failed to set up global compact subscriber");
        }
        LoggerFormat::Full => {
            tracing::subscriber::set_global_default(
                tracing_subscriber::registry().with(tracing_layer),
            )
            .expect("Failed to set up global compact subscriber");
        }
    };
    info!(?verbosity, ?format, "Initialized logger");
}
