use http::Uri;
use hyper::{Body, Response};
use mongodb::{options::ClientOptions, Client, Collection};
use once_cell::sync::Lazy;
use rand::distributions::{Alphanumeric, DistString};
use serde_json::Value;
use std::{
    net::{SocketAddr, TcpListener},
    thread,
};
use tokio::{runtime::Runtime, sync::oneshot};
use tracing::debug;

use crate::{
    config::MongoDBConfig,
    db::{Admin, Db},
    router::get_router,
};

const MONGO_ROOT_CONN_STR: &str = "mongodb://root:vEoZs9tEk3jLqS76@localhost:27017";
static MONGO_LOCAL_DB_CONFIG: Lazy<MongoDBConfig> = Lazy::new(|| MongoDBConfig {
    srv: false,
    host: "localhost:27017".to_string(),
    authentication_database: String::new(),
    user: "root".to_string(),
    password: "vEoZs9tEk3jLqS76".to_string(),
    database: String::from("book-admin"),
    replica_set: String::new(),
});

pub async fn body_as_string<T>(r: hyper::Response<T>) -> String
where
    T: axum::body::HttpBody,
    T::Error: std::fmt::Debug,
{
    let into = r.into_body();

    String::from_utf8(hyper::body::to_bytes(into).await.unwrap().to_vec()).unwrap()
}

pub async fn body_as_json<T>(r: hyper::Response<T>) -> Value
where
    T: axum::body::HttpBody,
    T::Error: std::fmt::Debug,
{
    let into = r.into_body();
    let body: Value = serde_json::from_slice(&hyper::body::to_bytes(into).await.unwrap()).unwrap();
    body
}

/// Retries the request continously for n seconds
pub async fn retry_request(uri: Uri, n: u32) -> Option<Response<Body>> {
    for _ in 0..n * 2 {
        let response = hyper::Client::new().get(uri.clone()).await;
        if let Ok(resp) = response {
            return Some(resp);
        }
        tokio::time::sleep(std::time::Duration::from_millis(500)).await;
    }
    None
}

/// Starts the server in separate thread and waits until it makes a simplest response.
/// The thread is automatically terminated when returned sender goes out of scope.
pub async fn start_background_server(listener: TcpListener, db_name: &str) -> oneshot::Sender<()> {
    let addr = listener.local_addr().unwrap();

    let cfg = get_mongo_cfg(db_name);
    let db = Db::new(Db::connect(&cfg).await.unwrap(), cfg.database)
        .await
        .unwrap();
    let router = get_router(db);
    // Starting our app in a background thread, rx automatically closes it when we go out of scope
    let (tx, rx) = oneshot::channel::<()>();
    thread::spawn(move || {
        Runtime::new().unwrap().block_on(async move {
            let server = axum::Server::from_tcp(listener)
                .unwrap()
                .serve(router.into_make_service());
            tokio::select! {
                _ = rx => {},
                _ = server => {},
            }
        });
    });
    // wait for the server to start responding
    retry_request(uri(addr, "/v1/health"), 5).await.unwrap();
    tx
}

pub fn uri(addr: SocketAddr, path_and_query: &str) -> Uri {
    let mut parts = http::uri::Parts::default();
    parts.scheme = Some("http".parse().unwrap());
    parts.authority = Some(addr.to_string().parse().unwrap());
    parts.path_and_query = Some(path_and_query.parse().unwrap());
    Uri::from_parts(parts).unwrap()
}

/// Starts a listener on a random port
/// Returns listener instance and corresponding socket address
pub fn get_random_port_listener(ip: &str) -> (TcpListener, SocketAddr) {
    let listener = TcpListener::bind((ip, 0)).unwrap();
    let addr = listener.local_addr().unwrap();
    (listener, addr)
}

pub fn get_mongo_cfg(database: &str) -> MongoDBConfig {
    let mut m = MONGO_LOCAL_DB_CONFIG.clone();
    m.database = String::from(database);
    m
}

pub async fn create_random_database() -> String {
    let new_database = Alphanumeric.sample_string(&mut rand::thread_rng(), 63);

    let client_options = ClientOptions::parse(MONGO_ROOT_CONN_STR).await.unwrap();
    let client = Client::with_options(client_options).unwrap();

    insert_test_data(&client, &new_database).await;

    new_database
}

pub async fn insert_test_data(client: &Client, db: &str) {
    let db = client.database(db);
    db.create_collection("admins", None).await.unwrap();
    let col: Collection<Admin> = db.collection::<Admin>("admins");

    let a = Admin {
        id: None,
        email: "email@domain.com".to_string(),
        first_name: "firstname".to_string(),
        last_name: "lastname".to_string(),
        password_hash: "asdfasdf".to_string(),
        version: 1,
    };
    let a2 = Admin {
        id: None,
        email: "email2@domain.com".to_string(),
        first_name: "firstname2".to_string(),
        last_name: "lastname2".to_string(),
        password_hash: "asdfasdf2".to_string(),
        version: 1,
    };
    col.insert_many([a, a2], None).await.unwrap();
}

pub async fn drop_database(name: &str) {
    let client_options = ClientOptions::parse(MONGO_ROOT_CONN_STR).await.unwrap();
    let client = Client::with_options(client_options).unwrap();

    debug!("dropping db debug {name}");
    let db = client.database(name);
    let t = db.drop(None).await;
    t.unwrap();
}
