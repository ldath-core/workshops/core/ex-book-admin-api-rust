use anyhow::Result;
use async_trait::async_trait;
#[cfg(test)]
use mockall::{mock, predicate::str};
use mongodb::bson::oid::ObjectId;

use crate::{
    db::Admin,
    router::{NewAdmin, UpdateAdmin},
};

/// Represents database subpart of App
#[async_trait]
pub trait DatabaseHandler: DatabaseConnector + AdminRepo {}

/// Represents interface for interacting with admins database
#[async_trait]
pub trait AdminRepo {
    async fn count_admins(&self, email: Option<String>) -> Result<i64>;
    async fn get_admins(&self, skip: i32, limit: i32, email: Option<String>) -> Result<Vec<Admin>>;
    async fn get_admin(&self, id: ObjectId) -> Result<Admin>;
    async fn create_admin(&self, b: NewAdmin) -> Result<Admin>;
    async fn delete_admin(&self, id: ObjectId) -> Result<u64>;
    async fn update_admin(&self, id: ObjectId, b: UpdateAdmin) -> Result<(u64, Option<String>)>;
}

/// Represents interface for initialization and managemend of the database connection
#[async_trait]
pub trait DatabaseConnector {
    async fn create_test_data(&self) -> Result<()>;
    async fn migrate_db(&self) -> Result<()>;
    async fn ping(&self) -> bool;
    async fn close(&self);
}

#[cfg(test)]
mock! {
    pub DatabaseHandler {}
    impl DatabaseHandler for DatabaseHandler {}
    #[async_trait]
    impl DatabaseConnector for DatabaseHandler {
        async fn create_test_data(&self) -> Result<()>;
        async fn migrate_db(&self) -> Result<()>;
        async fn ping(&self) -> bool;
        async fn close(&self);
    }
    #[async_trait]
    impl AdminRepo for DatabaseHandler {
        async fn count_admins(&self, email: Option<String>) -> Result<i64>;
        async fn get_admins(&self, skip: i32, limit: i32, email: Option<String>) -> Result<Vec<Admin>>;
        async fn get_admin(&self, id: ObjectId) -> Result<Admin>;
        async fn create_admin(&self, b: NewAdmin) -> Result<Admin>;
        async fn delete_admin(&self, id: ObjectId) -> Result<u64>;
        async fn update_admin(&self, id: ObjectId, b: UpdateAdmin) -> Result<(u64, Option<String>)>;
    }
}

#[cfg(test)]
use std::sync::Arc;
#[cfg(test)]
pub struct MockDatabaseHandlerWrapper {
    inner: Arc<MockDatabaseHandler>,
}
#[cfg(test)]
impl MockDatabaseHandlerWrapper {
    pub fn new(inner: MockDatabaseHandler) -> MockDatabaseHandlerWrapper {
        MockDatabaseHandlerWrapper {
            inner: Arc::new(inner),
        }
    }
}
#[cfg(test)]
impl Clone for MockDatabaseHandlerWrapper {
    fn clone(&self) -> Self {
        MockDatabaseHandlerWrapper {
            inner: self.inner.clone(),
        }
    }
}
#[cfg(test)]
impl DatabaseHandler for MockDatabaseHandlerWrapper {}
#[cfg(test)]
#[async_trait]
impl AdminRepo for MockDatabaseHandlerWrapper {
    async fn count_admins(&self, email: Option<String>) -> Result<i64> {
        self.inner.count_admins(email).await
    }
    async fn get_admins(&self, skip: i32, limit: i32, email: Option<String>) -> Result<Vec<Admin>> {
        self.inner.get_admins(skip, limit, email).await
    }
    async fn get_admin(&self, id: ObjectId) -> Result<Admin> {
        self.inner.get_admin(id).await
    }
    async fn create_admin(&self, b: NewAdmin) -> Result<Admin> {
        self.inner.create_admin(b).await
    }
    async fn delete_admin(&self, id: ObjectId) -> Result<u64> {
        self.inner.delete_admin(id).await
    }
    async fn update_admin(&self, id: ObjectId, b: UpdateAdmin) -> Result<(u64, Option<String>)> {
        self.inner.update_admin(id, b).await
    }
}
#[cfg(test)]
#[async_trait]
impl DatabaseConnector for MockDatabaseHandlerWrapper {
    async fn create_test_data(&self) -> Result<()> {
        Ok(())
    }
    async fn migrate_db(&self) -> Result<()> {
        Ok(())
    }
    async fn ping(&self) -> bool {
        self.inner.ping().await
    }
    async fn close(&self) {}
}
