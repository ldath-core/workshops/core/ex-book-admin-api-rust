use anyhow::Result;
use mongodb::bson::doc;

use crate::{
    config::MongoDBConfig,
    db::Db,
    router::{NewAdmin, UpdateAdmin},
    traits::AdminRepo,
};

const INITIALIZE_SUBSCRIBER: bool = false;

#[ctor::ctor]
fn setup() {
    if INITIALIZE_SUBSCRIBER {
        tracing_subscriber::fmt()
            .with_max_level(tracing::Level::TRACE)
            .init();
    };
    let r = tokio::runtime::Runtime::new().unwrap();
    r.block_on(async {
        let config = get_config();
        let db = Db::new(Db::connect(&config).await.unwrap(), config.database)
            .await
            .unwrap();
        db.drop_admins_collection().await.unwrap();
    });
}

#[tokio::test]
async fn test_create_admin() -> Result<()> {
    let b = get_new_admin();

    let config = get_config();
    let db = Db::new(Db::connect(&config).await?, config.database).await?;

    let t = db.create_admin(b.clone()).await?;
    assert_eq!(t.first_name, b.first_name.unwrap_or(String::new()));
    Ok(())
}

#[tokio::test]
async fn test_create_then_get_admins() -> Result<()> {
    let b = get_new_admin();

    let config = get_config();
    let db = Db::new(Db::connect(&config).await?, config.database).await?;

    db.create_admin(b.clone()).await.unwrap();

    let a = db.get_admins(0, 100, None).await.unwrap();
    assert!(!a.is_empty());

    Ok(())
}

#[tokio::test]
async fn test_create_then_get_admins_by_email() -> Result<()> {
    let generic_admin = get_new_admin;
    let tested_queried_newadmin = get_queried_newadmin;
    let queried_email = tested_queried_newadmin().email;
    let n_queried = 3;

    let config = get_config();
    let db = Db::new(Db::connect(&config).await?, config.database).await?;

    for _ in 0..2 {
        db.create_admin(generic_admin()).await?;
    }
    for _ in 0..n_queried {
        db.create_admin(tested_queried_newadmin()).await?;
    }

    let a = db
        .get_admins(0, 100, Some(queried_email.clone()))
        .await
        .unwrap();
    assert_eq!(a.len(), n_queried);
    for admin in &a {
        assert_eq!(admin.email, queried_email);
    }

    Ok(())
}

#[tokio::test]
async fn test_create_then_get_created() -> Result<()> {
    let b = get_new_admin();

    let config = get_config();
    let db = Db::new(Db::connect(&config).await?, config.database).await?;

    let t = db.create_admin(b.clone()).await?;

    let a = db.get_admin(t.id.unwrap()).await.unwrap();
    assert!(a.id == t.id);

    Ok(())
}

#[tokio::test]
async fn test_create_then_delete_created() -> Result<()> {
    let b = get_new_admin();

    let config = get_config();
    let db = Db::new(Db::connect(&config).await?, config.database).await?;

    let t = db.create_admin(b.clone()).await?;

    let d = db.delete_admin(t.id.unwrap()).await.unwrap();
    assert_eq!(d, 1);

    let a = db.get_admin(t.id.unwrap()).await;
    assert!(a.is_err());

    Ok(())
}

#[tokio::test]
async fn test_create_then_update_first_name_field() -> Result<()> {
    let b = get_new_admin();

    let config = get_config();
    let db = Db::new(Db::connect(&config).await?, config.database).await?;

    let t = db.create_admin(b.clone()).await?;
    assert_eq!(t.version, 1);

    let id = t.id.unwrap();
    let update = UpdateAdmin {
        email: None,
        first_name: Some("updated".to_string()),
        last_name: None,
        password: None,
        version: None,
    };
    let a = db.update_admin(id, update.clone()).await.unwrap();
    assert_eq!(a, (1, None));

    let a = db.get_admin(id).await.unwrap();
    assert_eq!(a.first_name, update.first_name.unwrap());
    assert_eq!(a.version, 1);

    Ok(())
}

#[tokio::test]
async fn test_create_then_update_password_field() -> Result<()> {
    let b = get_new_admin();

    let config = get_config();
    let db = Db::new(Db::connect(&config).await?, config.database).await?;

    let t = db.create_admin(b.clone()).await?;
    assert_eq!(t.version, 1);

    let id = t.id.unwrap();
    let update = UpdateAdmin {
        email: None,
        first_name: None,
        last_name: None,
        password: Some("updated".to_string()),
        version: None,
    };
    let a = db.update_admin(id, update.clone()).await.unwrap();
    assert_eq!(a.0, 1);
    assert!(a.1.is_some());

    let a = db.get_admin(id).await.unwrap();
    assert_eq!(a.first_name, t.first_name);
    assert_eq!(a.version, 1);
    assert!(Db::verify_password("updated", &a.password_hash).unwrap());

    Ok(())
}

#[tokio::test]
async fn test_create_then_update_all_fields() -> Result<()> {
    let b = get_new_admin();

    let config = get_config();
    let db = Db::new(Db::connect(&config).await?, config.database).await?;

    let t = db.create_admin(b.clone()).await?;
    assert_eq!(t.version, 1);

    let id = t.id.unwrap();
    let update = UpdateAdmin {
        email: Some("updatedemail".to_string()),
        first_name: Some("updatedfirstname".to_string()),
        last_name: Some("updatedlastname".to_string()),
        password: Some("updated".to_string()),
        version: None,
    };
    let a = db.update_admin(id, update.clone()).await.unwrap();
    assert_eq!(a.0, 1);
    assert!(a.1.is_some());

    let a = db.get_admin(id).await.unwrap();
    assert_eq!(a.first_name, update.first_name.unwrap());
    assert_eq!(a.last_name, update.last_name.unwrap());
    assert_eq!(a.email, update.email.unwrap());
    assert_eq!(a.version, 1);

    Ok(())
}

#[tokio::test]
async fn test_create_then_try_update_no_fields() -> Result<()> {
    let b = get_new_admin();

    let config = get_config();
    let db = Db::new(Db::connect(&config).await?, config.database).await?;

    let t = db.create_admin(b.clone()).await?;
    assert_eq!(t.version, 1);

    let id = t.id.unwrap();
    let update = UpdateAdmin {
        email: None,
        first_name: None,
        last_name: None,
        password: None,
        version: None,
    };
    let a = db.update_admin(id, update.clone()).await;
    assert!(a.is_err());
    Ok(())
}

fn get_config() -> MongoDBConfig {
    MongoDBConfig {
        srv: false,
        host: "localhost:27017".to_string(),
        authentication_database: "book-admin".to_string(),
        user: "book-admin".to_string(),
        password: "2vQU45haQCnDS8sO".to_string(),
        database: "book-admin".to_string(),
        replica_set: String::new(),
    }
}

fn get_queried_newadmin() -> NewAdmin {
    NewAdmin {
        email: "queried@site.com".to_string(),
        first_name: Some("firstname".to_string()),
        last_name: Some("lastname".to_string()),
        password: "asdfasdf".to_string(),
        version: None,
    }
}

fn get_new_admin() -> NewAdmin {
    NewAdmin {
        email: "email@domain.com".to_string(),
        first_name: Some("firstname".to_string()),
        last_name: Some("lastname".to_string()),
        password: "password".to_string(),
        version: None,
    }
}
