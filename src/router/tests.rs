use axum::{body::Body, http::Request};
use http::StatusCode;
use mongodb::bson::oid::ObjectId;
use serde::Serialize;
use serde_json::{json, Value};
use std::str::FromStr;
use tower::ServiceExt;

use super::*;
use crate::{
    db::Admin,
    tests::utils::{body_as_json, body_as_string},
    traits::{MockDatabaseHandler, MockDatabaseHandlerWrapper},
};

#[tokio::test]
async fn test_get_health() {
    let expected_status = StatusCode::OK;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db.expect_ping().returning(|| true);

    let req = Request::get("/v1/health").body(Body::empty()).unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);
}

#[tokio::test]
async fn test_wrong_uri() {
    let expected_status = StatusCode::NOT_FOUND;

    let req = Request::get("/nonexistent").body(Body::empty()).unwrap();
    let resp = get_router(wrap_mock(MockDatabaseHandler::new()))
        .oneshot(req)
        .await
        .unwrap();

    assert_eq!(resp.status(), expected_status);
}

#[tokio::test]
async fn test_get_admins_no_pagination() {
    let expected_status = StatusCode::OK;
    let tested_admins = get_vec_admins;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db
        .expect_count_admins()
        .returning(move |_| Ok(tested_admins().len() as i64));
    mock_db
        .expect_get_admins()
        .returning(move |_, _, _| Ok(tested_admins()));

    let req = Request::get("/v1/admins").body(Body::empty()).unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
    let inserted = get_vec_admins();
    for returned in body["content"]["results"].as_array().unwrap().iter() {
        let expected = inserted
            .iter()
            .find(|x| x.id.unwrap().to_string() == returned["id"].as_str().unwrap());

        let expect = serde_json::to_value(expected).unwrap();
        for (k, v) in expect.as_object().unwrap().iter() {
            assert_eq!(&returned[k], v);
        }
    }
}

#[tokio::test]
async fn test_pagination_only_skip() {
    let expected_status = StatusCode::OK;
    let skip = 1337;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db.expect_count_admins().returning(move |_| Ok(0));
    mock_db
        .expect_get_admins()
        .returning(move |_, _, _| Ok(Vec::<Admin>::new()));

    let req = Request::get(format!("/v1/admins?skip={skip}"))
        .body(Body::empty())
        .unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
    assert_eq!(body["content"]["skip"], skip);
}

#[tokio::test]
async fn test_pagination_limit_below_one() {
    let expected_status = StatusCode::BAD_REQUEST;
    let limit = 0;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db.expect_count_admins().returning(move |_| Ok(0));
    mock_db
        .expect_get_admins()
        .returning(move |_, _, _| Ok(Vec::<Admin>::new()));

    let req = Request::get(format!("/v1/admins?limit={limit}"))
        .body(Body::empty())
        .unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
    assert_eq!(body["content"], Value::Null);
}

#[tokio::test]
async fn test_get_admins_with_limit_and_email() {
    let expected_status = StatusCode::OK;
    let limit = 2;
    let email = "tested@email.com";

    let mut mock_db = MockDatabaseHandler::new();
    mock_db.expect_count_admins().returning(move |_| Ok(0));
    mock_db
        .expect_get_admins()
        .returning(move |_, _, _| Ok(Vec::<Admin>::new()));

    let req = Request::get(format!("/v1/admins?email={email}&limit={limit}"))
        .body(Body::empty())
        .unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
    assert!(body["content"].is_object());
}

#[tokio::test]
async fn test_get_existing_admin() {
    let expected_status = StatusCode::OK;
    let tested_admin = get_admin;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db
        .expect_get_admin()
        .returning(move |_| Ok(tested_admin()));

    let req = Request::get("/v1/admins/6410d35db27b00e0ed837372")
        .body(Body::empty())
        .unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());

    let original = serde_json::to_value(tested_admin()).unwrap();
    for (k, v) in original.as_object().unwrap().iter() {
        assert_eq!(&body["content"][k], v);
    }
}

#[tokio::test]
async fn test_get_admin_with_null_values() {
    let expected_status = StatusCode::OK;
    let tested_admin = get_admin_with_nones;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db
        .expect_get_admin()
        .returning(move |_| Ok(tested_admin()));

    let req = Request::get("/v1/admins/6410d35db27b00e0ed837372")
        .body(Body::empty())
        .unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_string(resp).await;
    assert!(!body.contains(":null,"));
}

#[tokio::test]
async fn test_create_admin() {
    let expected_status = StatusCode::CREATED;
    let tested_newadmin = get_new_admin;
    let tested_admin = get_admin_for_new_admin;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db
        .expect_create_admin()
        .returning(move |_| Ok(tested_admin()));

    let req = Request::post("/v1/admins")
        .header("content-type", "application/json")
        .body(Body::from(
            serde_json::to_vec(dbg!(&as_json(tested_newadmin()))).unwrap(),
        ))
        .unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());

    let original = serde_json::to_value(tested_admin()).unwrap();
    for (k, v) in original.as_object().unwrap().iter() {
        assert_eq!(&body["content"][k], v);
    }
}

#[tokio::test]
async fn test_create_admin_wrong_json() {
    let expected_status = StatusCode::UNPROCESSABLE_ENTITY;

    let req = Request::post("/v1/admins")
        .header("content-type", "application/json")
        .body(Body::from("{}{}}}}Not a correct json"))
        .unwrap();
    let resp = get_router(wrap_mock(MockDatabaseHandler::new()))
        .oneshot(req)
        .await
        .unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
    assert_eq!(body["content"], Value::Null);
}

#[tokio::test]
async fn test_create_admin_empty_fields() {
    let expected_status = StatusCode::NOT_ACCEPTABLE;
    let tested_admin_json = get_newadmin_empty_json;

    let router = get_router(wrap_mock(MockDatabaseHandler::new()));

    let req = Request::post("/v1/admins")
        .header("content-type", "application/json")
        .body(Body::from(
            serde_json::to_vec(&tested_admin_json()).unwrap(),
        ))
        .unwrap();
    let resp = router.oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
}

#[tokio::test]
async fn test_get_non_existent_admin() {
    let expected_status = StatusCode::INTERNAL_SERVER_ERROR;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db
        .expect_get_admin()
        .returning(|_| Err(anyhow::Error::msg("whatever error")));

    let req = Request::get("/v1/admins/6410d35db27b00e0ed837372")
        .body(Body::empty())
        .unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["content"], Value::Null);
}

#[tokio::test]
async fn test_get_admin_wrong_id() {
    let expected_status = StatusCode::BAD_REQUEST;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db
        .expect_get_admin()
        .returning(|_| Err(anyhow::Error::msg("whatever error")));

    let req = Request::get("/v1/admins/notanobjectid")
        .body(Body::empty())
        .unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["content"], Value::Null);
    assert_eq!(body["status"], expected_status.as_u16());
}

#[tokio::test]
async fn test_update_admin() {
    let expected_status = StatusCode::ACCEPTED;
    let tested_update_admin = get_new_admin;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db
        .expect_update_admin()
        .returning(move |_, _| Ok((1, Some("some_hash".to_string()))));

    let req = Request::put("/v1/admins/6410d35db27b00e0ed837372")
        .header("content-type", "application/json")
        .body(Body::from(
            serde_json::to_vec(&as_json(tested_update_admin())).unwrap(),
        ))
        .unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;
    let tested = tested_update_admin();

    assert_eq!(body["content"]["email"].as_str().unwrap(), tested.email);
    assert_eq!(
        body["content"]["passwordHash"].as_str().unwrap(),
        "some_hash".to_string()
    );
}

#[tokio::test]
async fn test_update_admin_wrong_json() {
    let expected_status = StatusCode::BAD_REQUEST;

    let req = Request::put("/v1/admins/6410d35db27b00e0ed837372")
        .header("content-type", "application/json")
        .body(Body::from("{{}{ Not a correct json"))
        .unwrap();
    let resp = get_router(wrap_mock(MockDatabaseHandler::new()))
        .oneshot(req)
        .await
        .unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
    assert_eq!(body["content"], Value::Null);
}

#[tokio::test]
async fn test_update_admin_empty_fields() {
    let expected_status = StatusCode::NOT_ACCEPTABLE;
    let tested_admin_json = get_newadmin_empty_json;

    let req = Request::put("/v1/admins/6410d35db27b00e0ed837372")
        .header("content-type", "application/json")
        .body(Body::from(
            serde_json::to_vec(&tested_admin_json()).unwrap(),
        ))
        .unwrap();
    let resp = get_router(wrap_mock(MockDatabaseHandler::new()))
        .oneshot(req)
        .await
        .unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
}

#[tokio::test]
async fn test_delete_admin() {
    let expected_status = StatusCode::ACCEPTED;
    let expected_message = "admin: 1 deleted";

    let mut mock_db = MockDatabaseHandler::new();
    mock_db.expect_delete_admin().returning(|_| Ok(1));

    let req = Request::delete("/v1/admins/6410d35db27b00e0ed837372")
        .header("content-type", "application/json")
        .body(Body::empty())
        .unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
    assert_eq!(body["message"], expected_message);
}

#[tokio::test]
async fn test_delete_non_existent_admin() {
    let expected_status = StatusCode::NOT_FOUND;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db.expect_delete_admin().returning(|_| Ok(0));

    let req = Request::delete("/v1/admins/6410d35db27b00e0ed837372")
        .body(Body::empty())
        .unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
}

fn get_vec_admins() -> Vec<Admin> {
    vec![
        Admin {
            id: Some(ObjectId::from_str("6411d587142bdb008dbd6f57").unwrap()),
            email: "email1@domain.com".to_string(),
            first_name: "firstname1".to_string(),
            last_name: "lastname1".to_string(),
            password_hash: "asdfasdf1".to_string(),
            version: 1,
        },
        Admin {
            id: Some(ObjectId::from_str("6411d587142bdb008dbd6f58").unwrap()),
            email: "email2@domain.com".to_string(),
            first_name: "firstname2".to_string(),
            last_name: "lastname2".to_string(),
            password_hash: "asdfasdf2".to_string(),
            version: 2,
        },
        Admin {
            id: Some(ObjectId::from_str("6411d587142bdb008dbd6f59").unwrap()),
            email: "email3@domain.com".to_string(),
            first_name: "firstname3".to_string(),
            last_name: "lastname3".to_string(),
            password_hash: "asdfasdf3".to_string(),
            version: 3,
        },
    ]
}

fn get_admin() -> Admin {
    Admin {
        id: None,
        email: "email0@domain.com".to_string(),
        first_name: "firstname0".to_string(),
        last_name: "lastname0".to_string(),
        password_hash: "asdfasdf0".to_string(),
        version: 1,
    }
}

fn get_new_admin() -> NewAdmin {
    NewAdmin {
        email: "emailN@domain.com".to_string(),
        first_name: Some("firstnameN".to_string()),
        last_name: Some("lastnameN".to_string()),
        password: "asdfasdfN".to_string(),
        version: None,
    }
}

fn get_admin_for_new_admin() -> Admin {
    Admin {
        id: None,
        email: "emailN@domain.com".to_string(),
        first_name: "firstnameN".to_string(),
        last_name: "lastnameN".to_string(),
        password_hash: "asdfasdfN".to_string(),
        version: 1,
    }
}

fn get_admin_with_nones() -> Admin {
    Admin {
        id: None,
        email: "email0@domain.com".to_string(),
        first_name: String::new(),
        last_name: String::new(),
        password_hash: "asdfasdf0".to_string(),
        version: 1,
    }
}

fn as_json<T: Serialize>(b: T) -> Value {
    serde_json::to_value(b).unwrap()
}

fn get_newadmin_empty_json() -> Value {
    json!({ "email": "", "password": "" })
}

fn wrap_mock(m: MockDatabaseHandler) -> MockDatabaseHandlerWrapper {
    MockDatabaseHandlerWrapper::new(m)
}
