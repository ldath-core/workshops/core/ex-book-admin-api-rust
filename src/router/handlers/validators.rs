use once_cell::sync::OnceCell;
use regex::Regex;

static RE: OnceCell<Regex> = OnceCell::new();
const EMAIL_REGEX: &str = r"\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,7}\b";

pub fn validate_email(email: &str) -> bool {
    let r = RE.get_or_init(|| Regex::new(EMAIL_REGEX).unwrap());
    r.is_match(email)
}
