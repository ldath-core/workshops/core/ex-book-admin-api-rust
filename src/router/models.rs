use serde::{Deserialize, Serialize};
use serde_json::Value;

/// Template which is converted into json and sent as a response to every
/// request
#[derive(Serialize)]
pub struct ApiResponse {
    pub status: u16,
    pub message: String,
    pub content: Option<Value>,
}

/// Inserted into [`ApiResponse::content`] as a response to requests that require
/// paginated content
#[derive(Serialize)]
pub(super) struct PaginatedContent {
    pub(super) count: i64,
    pub(super) skip: i32,
    pub(super) limit: i32,
    pub(super) results: Value,
}

/// Query template for handlers that support paginated content like
/// [`super::handlers::get_books`]
#[derive(Deserialize)]
pub(super) struct Pagination {
    pub(super) skip: Option<i32>,
    pub(super) limit: Option<i32>,
}

#[derive(Deserialize)]
pub(super) struct Email {
    pub(super) email: String,
}

#[cfg_attr(test, derive(Clone, Serialize))]
#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct NewAdmin {
    pub email: String,
    pub first_name: Option<String>,
    pub last_name: Option<String>,
    pub password: String,
    pub version: Option<i32>,
}

#[cfg_attr(test, derive(Clone))]
#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct UpdateAdmin {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub email: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub first_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub last_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub password: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub version: Option<i32>,
}
