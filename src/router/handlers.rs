//! Contains handlers used by the admin server
use std::sync::Arc;

use axum::{
    extract::{Query, State},
    http::{Request, StatusCode},
    middleware::Next,
    response::IntoResponse,
};
use mongodb::bson::oid::ObjectId;
use serde_json::json;
use tracing::{debug, error, info};

mod extractors;
mod validators;

use super::{
    macros::{response, unwrap_or_return},
    models::{ApiResponse, Email, NewAdmin, PaginatedContent, Pagination},
    UpdateAdmin,
};
use crate::traits::DatabaseHandler;
use extractors::{Json, Path};
use validators::validate_email;

pub(super) async fn not_found() -> impl IntoResponse {
    (
        StatusCode::NOT_FOUND,
        axum::Json(ApiResponse {
            status: StatusCode::NOT_FOUND.as_u16(),
            message: "Page Not Found".to_owned(),
            content: None,
        }),
    )
}

pub async fn method_not_allowed<B>(req: Request<B>, next: Next<B>) -> impl IntoResponse {
    let resp = next.run(req).await;
    let status = resp.status();
    match status {
        StatusCode::METHOD_NOT_ALLOWED => (
            StatusCode::METHOD_NOT_ALLOWED,
            axum::Json(ApiResponse {
                status: StatusCode::METHOD_NOT_ALLOWED.as_u16(),
                message: "Method not allowed".to_owned(),
                content: None,
            }),
        )
            .into_response(),
        _ => resp,
    }
}

pub(super) async fn get_health<D: DatabaseHandler>(State(db): State<Arc<D>>) -> impl IntoResponse {
    // pub(super) async fn get_health<D: DatabaseHandler>() -> impl IntoResponse {
    response!(
        StatusCode::OK,
        "book-admin api health",
        Some(json!({"alive": true, "mongo": db.ping().await })) // Some(json!({"alive": true, "mongo": true }))
    )
}

pub(super) async fn get_admins<D: DatabaseHandler>(
    State(db): State<Arc<D>>,
    pagination: Query<Pagination>,
    email: Option<Query<Email>>,
) -> impl IntoResponse {
    let skip = pagination.skip.unwrap_or(0);
    let limit = pagination.limit.unwrap_or(10);
    if skip < 0 || limit < 1 {
        return response!(
            StatusCode::BAD_REQUEST,
            "skip must be >= 0, limit >= 1",
            None
        );
    }
    let email = email.map(|Query(e)| e.email);
    if let Some(e) = email.as_ref() {
        if !validate_email(e) {
            return response!(
                StatusCode::NOT_ACCEPTABLE,
                "email is not formatted correctly",
                None
            );
        }
    }
    let b = unwrap_or_return!(
        db.get_admins(skip, limit, email.clone()).await,
        response!(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Problem with getting admins",
            None
        )
    );
    let count = unwrap_or_return!(
        db.count_admins(email).await,
        response!(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Problem with counting admins",
            None
        )
    );
    response!(
        StatusCode::OK,
        format!("admins - skip: {skip}; limit: {limit}"),
        Some(
            serde_json::to_value(PaginatedContent {
                count,
                skip,
                limit,
                results: serde_json::to_value(b).unwrap(),
            })
            .unwrap()
        )
    )
}

pub(super) async fn create_admin<D: DatabaseHandler>(
    State(db): State<Arc<D>>,
    Json(b): Json<NewAdmin>,
) -> impl IntoResponse {
    info!("create_admin request for {:?}", b);
    if b.email.is_empty() || b.password.is_empty() {
        return response!(
            StatusCode::NOT_ACCEPTABLE,
            "email and password fields cannot be empty",
            None
        );
    }
    let b = unwrap_or_return!(
        db.create_admin(b).await,
        response!(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Problem with creating admin",
            None
        )
    );

    response!(
        StatusCode::CREATED,
        "ok",
        Some(serde_json::to_value(b).unwrap())
    )
}

pub(super) async fn get_admin<D: DatabaseHandler>(
    State(db): State<Arc<D>>,
    Path(id): Path<ObjectId>,
) -> impl IntoResponse {
    debug!("get_admin request for id: {}", id);
    let b = unwrap_or_return!(
        db.get_admin(id).await,
        response!(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Problem with getting admin",
            None
        )
    );
    let j = serde_json::to_value(b).expect("serde_json couldn't hande Admin struct");
    response!(StatusCode::OK, "admin", Some(j))
}

pub(super) async fn update_admin<D: DatabaseHandler>(
    State(db): State<Arc<D>>,
    Path(id): Path<ObjectId>,
    Json(b): Json<UpdateAdmin>,
) -> impl IntoResponse {
    debug!("update_admin request for id: {} with new data {:?}", id, b);
    if let Some(v) = b.email.as_ref() {
        if v.is_empty() {
            return response!(StatusCode::NOT_ACCEPTABLE, "email cannot be empty", None);
        }
    }
    if let Some(v) = b.password.as_ref() {
        if v.is_empty() {
            return response!(StatusCode::NOT_ACCEPTABLE, "password cannot be empty", None);
        }
    }
    if let Some(e) = b.email.as_ref() {
        if !validate_email(e) {
            return response!(
                StatusCode::NOT_ACCEPTABLE,
                "email is not formatted correctly",
                None
            );
        }
    }
    let mut updated_content = serde_json::to_value(&b).unwrap();
    let r = unwrap_or_return!(
        db.update_admin(id, b).await,
        response!(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Problem with updating admin",
            None
        )
    );
    if let Some(s) = r.1 {
        let u = updated_content.as_object_mut().unwrap();
        u.insert("passwordHash".to_string(), serde_json::Value::String(s));
    };
    response!(
        StatusCode::ACCEPTED,
        format!("admin: {id} updated"),
        Some(updated_content)
    )
}

pub(super) async fn delete_admin<D: DatabaseHandler>(
    State(db): State<Arc<D>>,
    Path(id): Path<ObjectId>,
) -> impl IntoResponse {
    debug!("delete_admin request for id: {}", id);
    let b = unwrap_or_return!(
        db.delete_admin(id).await,
        response!(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Problem with deleting admin",
            None
        )
    );
    match b {
        0 => response!(StatusCode::NOT_FOUND, "admin not found", None),
        _ => response!(StatusCode::ACCEPTED, format!("admin: {b} deleted"), None),
    }
}
