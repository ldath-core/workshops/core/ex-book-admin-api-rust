use std::sync::Arc;

use axum::{middleware, routing::get, Router};
use tower_http::{
    cors::CorsLayer,
    trace::{DefaultOnRequest, DefaultOnResponse, TraceLayer},
};
use tracing::{info, Level};

mod handlers;
mod macros;
mod models;
#[cfg(test)]
mod tests;

use crate::traits::DatabaseHandler;
use handlers::{
    create_admin, delete_admin, get_admin, get_admins, get_health, method_not_allowed, not_found,
    update_admin,
};

pub use models::{NewAdmin, UpdateAdmin};

/// Return router configured for the admin API
pub fn get_router<D>(db: D) -> Router
where
    D: DatabaseHandler + Clone + Send + Sync + 'static,
{
    let router: Router = Router::new()
        .route("/health", get(get_health::<D>))
        .route("/admins", get(get_admins::<D>).post(create_admin::<D>))
        .route(
            "/admins/:id",
            get(get_admin::<D>)
                .put(update_admin::<D>)
                .patch(update_admin::<D>)
                .delete(delete_admin::<D>),
        )
        .layer(middleware::from_fn(method_not_allowed))
        .layer(
            TraceLayer::new_for_http()
                .on_request(DefaultOnRequest::new().level(Level::INFO))
                .on_response(DefaultOnResponse::new().level(Level::INFO)),
        )
        .with_state(Arc::new(db));
    Router::new().nest("/v1", router).fallback(not_found)
}

/// Add CORS layer to ther router
///
/// # Examples
///
/// ```ignore
///  # let router = get_router(db::Db})
///  router = add_cors_layer(router.clone());
/// ```
pub fn add_cors_layer(r: Router) -> Router {
    r.layer(CorsLayer::permissive())
}

pub async fn shutdown_signal() {
    tokio::signal::ctrl_c()
        .await
        .expect("Expect shutdown signal handler");
    info!("received shutdown signal");
}
