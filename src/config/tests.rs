use assert_fs::fixture::NamedTempFile;
use assert_fs::prelude::*;

use super::*;

#[test]
fn test_load_config() {
    let tmp_cfg = NamedTempFile::new("tmp_config.yaml").unwrap();
    tmp_cfg
        .write_str(
            r#"---
env: dev
mongodb:
    srv: False
    host: 'localhost:27017'
    authentication-database: 'book-admin'
    user: book-admin
    password: 'password123'
    database: 'book-admin'
    replica-set: ''
logger:
    level: debug
"#,
        )
        .unwrap();
    let c = load_config(tmp_cfg.path());
    assert!(c.env == "dev");
    assert!(c.logger.level == "debug");
    assert!(c.mongodb.password == "password123");
}
