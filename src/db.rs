use anyhow::Result;
use async_trait::async_trait;
use futures::stream::TryStreamExt;
use mongodb::{
    bson::{doc, oid::ObjectId},
    options::{ClientOptions, FindOptions, UpdateOptions},
    Client,
};
use tracing::{debug, error, instrument};

mod models;
#[cfg(test)]
mod tests;
use crate::{
    config::MongoDBConfig,
    router::{NewAdmin, UpdateAdmin},
    traits::{AdminRepo, DatabaseConnector, DatabaseHandler},
};
pub use models::Admin;

/// Contains DB connection and implements layer for interacting with books database
#[derive(Debug, Clone)]
pub struct Db {
    mongo: Client,
    admins_collection: mongodb::Collection<Admin>,
    database: String,
}

impl Db {
    #[instrument(skip_all, level = "trace")]
    pub async fn new(c: Client, database: String) -> Result<Self> {
        let admins_collection = c.database(&database).collection::<Admin>("admins");
        Ok(Self {
            mongo: c,
            admins_collection,
            database,
        })
    }

    #[instrument(skip_all, level = "trace")]
    pub async fn connect(config: &MongoDBConfig) -> Result<Client> {
        let conn_str = Self::get_conn_str(config);
        let mut client_options = ClientOptions::parse(conn_str).await?;
        client_options.default_database = Some(config.database.clone());
        let client = Client::with_options(client_options)?;
        Ok(client)
    }

    pub async fn drop_admins_collection(&self) -> Result<()> {
        let col = self.get_admins_collection();
        col.drop(None).await?;
        Ok(())
    }

    pub fn get_conn_str(c: &MongoDBConfig) -> String {
        let scheme = if c.srv { "mongodb+srv" } else { "mongodb" };
        let user = &c.user;
        let password = &c.password;
        let host = &c.host;
        let mut query = String::new();
        if !c.authentication_database.is_empty() {
            if query.is_empty() {
                query.push('?');
            }
            query.push_str(&format!("authSource={}", c.authentication_database));
        }
        if !c.replica_set.is_empty() {
            if query.is_empty() {
                query.push('?');
            } else {
                query.push('&');
            }
            query.push_str(&format!("replicaSet={}", &c.replica_set));
        }
        let conn_str = format!("{scheme}://{user}:{password}@{host}/{query}");
        conn_str
    }

    #[instrument(skip_all, level = "trace")]
    fn get_admins_collection(&self) -> mongodb::Collection<Admin> {
        // self.admins_collection.clone()
        self.mongo
            .database(&self.database)
            .collection::<Admin>("admins")
    }

    #[instrument(skip_all, level = "trace")]
    fn hash_password(password: String) -> Result<String, bcrypt::BcryptError> {
        bcrypt::hash(password, 14)
    }

    #[cfg(test)]
    pub fn verify_password(password: &str, hash: &str) -> Result<bool, bcrypt::BcryptError> {
        bcrypt::verify(password, hash)
    }
}

impl DatabaseHandler for Db {}

/// Abstract away DB internals exposing interface for interacting with books data
#[async_trait]
impl AdminRepo for Db {
    #[instrument(skip_all, level = "trace")]
    async fn count_admins(&self, email: Option<String>) -> Result<i64> {
        let col = self.get_admins_collection();
        let filter = email.map(|e| doc! {"email": e});
        let count = col.count_documents(filter, None).await?;
        Ok(count as i64)
    }

    #[instrument(skip_all, level = "trace")]
    async fn get_admins(&self, skip: i32, limit: i32, email: Option<String>) -> Result<Vec<Admin>> {
        let col = self.get_admins_collection();
        let opts = FindOptions::builder()
            .skip(Some(skip as u64))
            .limit(Some(i64::from(limit)))
            .sort(doc! { "_id": -1 })
            .build();
        let doc = email.map(|e| doc! {"email": e});
        debug!("retrieving admins from mongo with doc: {doc:?}");
        let res = col.find(doc, opts).await.unwrap();
        let admins: Vec<Admin> = res.try_collect().await?;
        Ok(admins)
    }

    #[instrument(skip_all, level = "trace")]
    async fn get_admin(&self, id: ObjectId) -> Result<Admin> {
        let col = self.get_admins_collection();
        let res = col.find_one(doc! { "_id": id }, None).await?;
        res.ok_or(anyhow::Error::msg("Admin with _id: {id:?} was not found"))
    }

    #[instrument(skip_all, level = "trace")]
    async fn create_admin(&self, b: NewAdmin) -> Result<Admin> {
        let col = self.get_admins_collection();
        let password_hash = Self::hash_password(b.password)?;
        let version = b.version.unwrap_or(1);
        let mut adm = Admin {
            id: None,
            email: b.email,
            first_name: b.first_name.unwrap_or(String::new()),
            last_name: b.last_name.unwrap_or(String::new()),
            password_hash,
            version,
        };
        let res = col.insert_one(adm.clone(), None).await?;

        adm.id = res.inserted_id.as_object_id();
        Ok(adm)
    }

    #[instrument(skip_all, level = "trace")]
    async fn delete_admin(&self, id: ObjectId) -> Result<u64> {
        let col = self.get_admins_collection();
        let res = col.delete_one(doc! { "_id": id }, None).await?;
        Ok(res.deleted_count)
    }

    #[instrument(skip_all, level = "trace")]
    async fn update_admin(&self, id: ObjectId, b: UpdateAdmin) -> Result<(u64, Option<String>)> {
        let col = self.get_admins_collection();
        debug!("updating admin {:?} into db, new ID: {}", b, id);
        let opts = UpdateOptions::builder().upsert(Some(false)).build();
        let mut update = doc! {
            "$set": { }
        };
        let s = update.get_document_mut("$set").unwrap();
        if let Some(v) = b.email {
            s.insert("email", v);
        }
        if let Some(v) = b.first_name {
            s.insert("firstName", v);
        }
        if let Some(v) = b.last_name {
            s.insert("lastName", v);
        }
        let mut password_hash = None;
        if let Some(v) = b.password {
            let h = Self::hash_password(v)?;
            s.insert("passwordHash", &h);
            password_hash = Some(h);
        }
        if s.is_empty() {
            return Err(anyhow::Error::msg("None of the fields were updated"));
        }
        let res = col.update_one(doc! { "_id": id }, update, opts).await?;

        Ok((res.modified_count, password_hash))
    }
}

#[async_trait]
impl DatabaseConnector for Db {
    #[instrument(skip_all, level = "trace")]
    async fn create_test_data(&self) -> Result<()> {
        let new_admins = vec![
            NewAdmin {
                email: "email1@domain.com".to_string(),
                first_name: Some("firstname1".to_string()),
                last_name: Some("lastname1".to_string()),
                password: "password1".to_string(),
                version: None,
            },
            NewAdmin {
                email: "email2@domain.com".to_string(),
                first_name: Some("firstname2".to_string()),
                last_name: Some("lastname2".to_string()),
                password: "password2".to_string(),
                version: None,
            },
            NewAdmin {
                email: "email3@domain.com".to_string(),
                first_name: Some("firstname3".to_string()),
                last_name: Some("lastname3".to_string()),
                password: "password3".to_string(),
                version: None,
            },
            NewAdmin {
                email: "atulodzi@gmail.com".to_string(),
                first_name: Some("Main".to_string()),
                last_name: Some("Administrator".to_string()),
                password: "NicePass4Me".to_string(),
                version: None,
            },
        ];

        std::thread::scope(|s| {
            for na in new_admins {
                let rt = tokio::runtime::Runtime::new().unwrap();
                s.spawn(move || {
                    rt.block_on(self.create_admin(na)).unwrap();
                });
            }
        });

        Ok(())
    }

    /// Sets up necessary underlaying database schema and tables
    #[instrument(skip_all, level = "trace")]
    async fn migrate_db(&self) -> Result<()> {
        self.drop_admins_collection().await?;
        debug!("Database migrated");
        Ok(())
    }

    /// Closes DB connection, makes self unusable
    #[instrument(skip_all, level = "trace")]
    async fn close(&self) {}

    /// Checks if DB is alive
    #[instrument(skip_all, level = "trace")]
    async fn ping(&self) -> bool {
        let q = self
            .mongo
            .database("admin")
            .run_command(doc! {"ping": 1}, None)
            .await;
        match q {
            Ok(_) => true,
            Err(e) => {
                error!("ping error {e}");
                false
            }
        }
    }
}
